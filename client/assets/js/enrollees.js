let token = localStorage.getItem("token");
let isAdmin = localStorage.getItem("isAdmin");
let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");

(isAdmin)
?	
	fetch(`http://localhost:3000/api/courses/${courseId}`)
	.then(res => res.json())
	.then(courseData => {
		console.log(courseData);
		/*
		Insert your code here to populate the values from variable courseData with object contents regarding your course information.
		*/

		//this codes below is to get the enrollees and each details.
		let enrolleesData = courseData.enrollees;
		let enrolleeData;
		for (enrolleeData of enrolleesData) {
			fetch(`http://localhost:3000/api/users/${enrolleeData.userId}`, {
				method: "GET",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(userData => {
				console.log(userData);
				/*
					Insert your code here to populate the enrollee details using variable userData.
				*/
			})
		}
	})
:
	window.location.replace("./courses.html");

