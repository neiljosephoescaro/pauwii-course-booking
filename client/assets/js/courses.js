let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector('#adminButton')
let container = document.querySelector('#coursesContainer')
let cardFooter, coursesData;

if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null
	fetch('http://localhost:3000/api/courses')
	.then(res => res.json())
	.then(data => {
		//if the number of courses fetched is less than 1, display no courses available.
		if(data.length < 1){
			coursesData = "No courses available."
		}else{
			coursesData = data.map(course => {
				cardFooter = 
				`
					<a href="./course.html?courseId=${course._id}" class="btn btn-dark text-white btn-pill editButton">Select Course</a>
				`
				return (
				`
					<div class="col-md-6 my-3">
						<div class="card text-white bg-dark mb-3">

							<div class="card-body">
								<img class="card-img-top" src="https://adventuretimenipaowii.files.wordpress.com/2020/09/20200916_140151-2.gif?w=350" alt="card img">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">₱ ${course.price}</p>
							</div>

							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
				)

			}).join("")
		}
		container.innerHTML = coursesData;
	})
}else{
	modalButton.innerHTML = `<div class="col-md-2 offset-md-10">
	<a href="./addCourse.html" class="btn btn-block btn-dark">Add Course</a>
	</div>`
	//added this codes below to view all courses even if it's inactive when user is admin
	let token = localStorage.getItem("token");
	fetch('http://localhost:3000/api/courses/all', {
		method: "GET",
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		if(data.length < 1){
			coursesData = "No courses available."
		}else{
			coursesData = data.map(course => {
				cardFooter = `<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-dark text-white btn-pill editButton">Edit</a>
							<a href="./archive.html?courseId=${course._id}" value=${course._id} class="btn btn-dark text-white btn-pill editButton">Archive</a>
							<a href="./reActivate.html?courseId=${course._id}" value=${course._id} class="btn btn-dark text-white btn-pill editButton">Reactivate</a>
							<a href="./enrollees.html?courseId=${course._id}" value=${course._id} class="btn btn-dark text-white btn-pill editButton">Enrollees</a>`
				return (
				`
					<div class="col-md-6 my-3">
						<div class="card text-white bg-dark mb-3">

							<div class="card-body">
								<img class="card-img-top" src="https://adventuretimenipaowii.files.wordpress.com/2020/09/20200916_140151-2.gif?w=350" alt="card img">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">₱ ${course.price}</p>
							</div>

							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
				)

			}).join("")
		}
		container.innerHTML = coursesData;
	})
}

