let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let token = localStorage.getItem("token");

fetch(`http://localhost:3000/api/courses/${courseId}`, {
	method : "PUT",
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then( res => res.json())
.then( data => {
	if(data === true){	
		alert("Reactivated successfully")		
			window.location.replace('./courses.html')
		}else{
			alert('Something went wrong.')
		}
	})
})

//rename the filename from archiveCourse to reActivate