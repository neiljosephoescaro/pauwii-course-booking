let params = new URLSearchParams(window.location.search);
let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin");


let userId = params.get('user')
let userName = document.querySelector("#userName");
let email = document.querySelector("#email");
let mobileNo = document.querySelector("#mobileNo");
let courses = document.querySelector("#courses");
let coursesEnrolled = document.querySelector("#coursesEnrolled");
let profileContainer = document.querySelector("#profileContainer");



fetch('http://localhost:3000/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(userData => {

	userName.innerHTML = userData.firstName + " " + userData.lastName
	email.innerHTML = userData.email
	mobileNo.innerHTML = userData.mobileNo

	let enrollmentsData = userData.enrollments;
	let enrollmentData;
	for (enrollmentData of enrollmentsData) {
		fetch(`http://localhost:3000/api/courses/${enrollmentData.courseId}`)
		.then( res => res.json())
		.then( courseData => {
			console.log(courseData);
			/*
			insert your code here to populate the enrolled courses of user using the variable courseData
			*/
		})
	}
	
})